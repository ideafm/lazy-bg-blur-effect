import config from '../config';

import gulp from 'gulp';
import pug from 'gulp-pug';
import gulpif from 'gulp-if';
import plumber from 'gulp-plumber';

let data = {
    jv0: 'javascript:void(0)'
};

const emitty = require('emitty').setup(config.PATH.pug.baseDir, 'pug');

gulp.task('watch:templates', () => {
    global.watch = true;

    gulp.watch(config.PATH.pug.watch, gulp.series('templates'))
        .on('all', (event, filepath) => {
            global.emittyChangedFile = filepath;
        });
});


gulp.task('templates', () =>
    new Promise((resolve, reject) => {
        emitty.scan(global.emittyChangedFile).then(() => {
            gulp.src(config.PATH.pug.src)
                .pipe(plumber())
                .pipe(gulpif(global.watch, emitty.filter(global.emittyChangedFile)))
                .pipe(pug({ pretty: true }))
                .pipe(gulp.dest(config.PATH.dest))
                .on('end', resolve)
                .on('error', reject);
        });
    })
);