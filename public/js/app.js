/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/js/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// foreach fixes
var forEach = function forEach(array, callback, scope) {
    for (var i = 0; i < array.length; i++) {
        callback.call(scope, array[i], i);
    }
};

// lazy bg blur effect

var BlurBg = function () {
    function BlurBg(rootEl) {
        _classCallCheck(this, BlurBg);

        this.svgns = 'http://www.w3.org/2000/svg';
        this.xlink = 'http://www.w3.org/1999/xlink';

        this.root = rootEl;
        this.img = rootEl.getAttribute('data-lazy-bg');
        this.imgThumb = this.getThumbName();

        var svg = this.createSVG();
        var filterId = this.generateFilterId();
        var filter = this.createFilter(filterId);
        var blur = this.createBlur();
        var image = this.createImage(this.imgThumb, filterId);

        filter.appendChild(blur);
        svg.appendChild(filter);
        svg.appendChild(image);

        rootEl.appendChild(svg);

        this.createImg(svg);
    }

    _createClass(BlurBg, [{
        key: 'getThumbName',
        value: function getThumbName() {
            var thumb = this.root.getAttribute('data-lazy-bg-thumb');
            if (!thumb) {
                var res = this.img.match(/\.[png|jpg|svg|jpeg]+$/);
                if (res) {
                    thumb = this.img.substr(0, res.index) + '_thumb' + res[0];
                }
            }
            return thumb;
        }
    }, {
        key: 'createImg',
        value: function createImg(svg) {
            var _this = this;

            var img = new Image();
            img.addEventListener('load', function (e) {
                _this.root.style.backgroundImage = 'url(\'' + _this.img + '\')';
                setTimeout(function () {
                    _this.fadeOut(svg, function (e) {
                        svg.parentNode.removeChild(svg);
                    });
                }, 500);
            });
            img.setAttribute('src', this.img);
        }
    }, {
        key: 'fadeOut',
        value: function fadeOut(el, cb) {
            el.style.opacity = 1;

            var last = +new Date();
            var tick = function tick() {
                el.style.opacity = +el.style.opacity - (new Date() - last) / 600;
                last = +new Date();

                if (+el.style.opacity > 0) {
                    window.requestAnimationFrame && requestAnimationFrame(tick) || setTimeout(tick, 16);
                } else {
                    if (typeof cb === 'function') {
                        cb();
                    }
                }
            };

            tick();
        }
    }, {
        key: 'createImage',
        value: function createImage(url, filterId) {
            return this.createElement('image', {
                x: 0,
                y: 0,
                width: '100%',
                height: '100%',
                'externalResourcesRequired': 'true',
                href: url,
                style: 'filter:url(#' + filterId + ')', //filter link
                preserveAspectRatio: 'xMidYMid slice'
            });
        }
    }, {
        key: 'createBlur',
        value: function createBlur() {
            return this.createElement('feGaussianBlur', {
                'in': 'SourceGraphic',
                stdDeviation: 10
            });
        }
    }, {
        key: 'createFilter',
        value: function createFilter(filterId) {
            return this.createElement('filter', {
                id: filterId
            });
        }
    }, {
        key: 'createSVG',
        value: function createSVG() {
            return this.createElement('svg', {
                width: '100%',
                height: '100%',
                xmlns: 'http://www.w3.org/2000/svg',
                version: '1.1',
                style: "position: absolute; z-index: 1; background: white; left: 0; top: 0;"
            });
        }
    }, {
        key: 'createElement',
        value: function createElement(name, attrs) {
            var el = document.createElementNS(this.svgns, name);

            if (attrs) {
                this.setSVGAttr(el, attrs);
            }

            return el;
        }
    }, {
        key: 'setSVGAttr',
        value: function setSVGAttr(el, attrs) {
            for (var i in attrs) {
                if (i === 'href') {
                    el.setAttributeNS(this.xlink, i, attrs[i]);
                } else {
                    el.setAttribute(i, attrs[i]);
                }
            }

            return el;
        }
    }, {
        key: 'generateFilterId',
        value: function generateFilterId() {
            var id = 'filter_' + Date.now();
            return id;
        }
    }]);

    return BlurBg;
}();

window.addEventListener('load', function () {
    forEach(document.querySelectorAll('[data-lazy-bg]'), function (item) {
        new BlurBg(item);
    });
});

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(0);


/***/ })
/******/ ]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3dlYnBhY2svYm9vdHN0cmFwIDkyOGVlYzIxNGIyNjY0M2VhNWU3Iiwid2VicGFjazovLy9zcmMvanMvYXBwLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiL2pzL1wiO1xuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IDEpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIDkyOGVlYzIxNGIyNjY0M2VhNWU3IiwiLy8gZm9yZWFjaCBmaXhlc1xubGV0IGZvckVhY2ggPSBmdW5jdGlvbiAoYXJyYXksIGNhbGxiYWNrLCBzY29wZSkge1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgYXJyYXkubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgY2FsbGJhY2suY2FsbChzY29wZSwgYXJyYXlbaV0sIGkpO1xuICAgIH1cbn07XG5cbi8vIGxhenkgYmcgYmx1ciBlZmZlY3RcbmNsYXNzIEJsdXJCZyB7XG4gICAgY29uc3RydWN0b3Iocm9vdEVsKSB7XG5cbiAgICAgICAgdGhpcy5zdmducyA9ICdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zyc7XG4gICAgICAgIHRoaXMueGxpbmsgPSAnaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayc7XG5cbiAgICAgICAgdGhpcy5yb290ID0gcm9vdEVsO1xuICAgICAgICB0aGlzLmltZyA9IHJvb3RFbC5nZXRBdHRyaWJ1dGUoJ2RhdGEtbGF6eS1iZycpO1xuICAgICAgICB0aGlzLmltZ1RodW1iID0gdGhpcy5nZXRUaHVtYk5hbWUoKTtcblxuICAgICAgICBsZXQgc3ZnID0gdGhpcy5jcmVhdGVTVkcoKTtcbiAgICAgICAgbGV0IGZpbHRlcklkID0gdGhpcy5nZW5lcmF0ZUZpbHRlcklkKCk7XG4gICAgICAgIGxldCBmaWx0ZXIgPSB0aGlzLmNyZWF0ZUZpbHRlcihmaWx0ZXJJZCk7XG4gICAgICAgIGxldCBibHVyID0gdGhpcy5jcmVhdGVCbHVyKCk7XG4gICAgICAgIGxldCBpbWFnZSA9IHRoaXMuY3JlYXRlSW1hZ2UodGhpcy5pbWdUaHVtYiwgZmlsdGVySWQpO1xuXG4gICAgICAgIGZpbHRlci5hcHBlbmRDaGlsZChibHVyKTtcbiAgICAgICAgc3ZnLmFwcGVuZENoaWxkKGZpbHRlcik7XG4gICAgICAgIHN2Zy5hcHBlbmRDaGlsZChpbWFnZSk7XG5cbiAgICAgICAgcm9vdEVsLmFwcGVuZENoaWxkKHN2Zyk7XG5cbiAgICAgICAgdGhpcy5jcmVhdGVJbWcoc3ZnKTtcblxuICAgIH1cblxuICAgIGdldFRodW1iTmFtZSgpIHtcbiAgICAgICAgbGV0IHRodW1iID0gdGhpcy5yb290LmdldEF0dHJpYnV0ZSgnZGF0YS1sYXp5LWJnLXRodW1iJyk7XG4gICAgICAgIGlmKCF0aHVtYikge1xuICAgICAgICAgICAgbGV0IHJlcyA9IHRoaXMuaW1nLm1hdGNoKC9cXC5bcG5nfGpwZ3xzdmd8anBlZ10rJC8pO1xuICAgICAgICAgICAgaWYocmVzKSB7XG4gICAgICAgICAgICAgICAgdGh1bWIgPSB0aGlzLmltZy5zdWJzdHIoMCwgcmVzLmluZGV4KSArICdfdGh1bWInICsgcmVzWzBdO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aHVtYjtcbiAgICB9XG5cbiAgICBjcmVhdGVJbWcoc3ZnKSB7XG4gICAgICAgIGxldCBpbWcgPSBuZXcgSW1hZ2UoKTtcbiAgICAgICAgaW1nLmFkZEV2ZW50TGlzdGVuZXIoJ2xvYWQnLCBlID0+IHtcbiAgICAgICAgICAgIHRoaXMucm9vdC5zdHlsZS5iYWNrZ3JvdW5kSW1hZ2UgPSBgdXJsKCcke3RoaXMuaW1nfScpYDtcbiAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuZmFkZU91dChzdmcsIGUgPT4ge1xuICAgICAgICAgICAgICAgICAgICBzdmcucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChzdmcpO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9LCA1MDApXG4gICAgICAgIH0pO1xuICAgICAgICBpbWcuc2V0QXR0cmlidXRlKCdzcmMnLCB0aGlzLmltZyk7XG4gICAgfVxuXG4gICAgZmFkZU91dChlbCwgY2IpIHtcbiAgICAgICAgZWwuc3R5bGUub3BhY2l0eSA9IDE7XG5cbiAgICAgICAgbGV0IGxhc3QgPSArbmV3IERhdGUoKTtcbiAgICAgICAgbGV0IHRpY2sgPSBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGVsLnN0eWxlLm9wYWNpdHkgPSArZWwuc3R5bGUub3BhY2l0eSAtIChuZXcgRGF0ZSgpIC0gbGFzdCkgLyA2MDA7XG4gICAgICAgICAgICBsYXN0ID0gK25ldyBEYXRlKCk7XG5cbiAgICAgICAgICAgIGlmICgrZWwuc3R5bGUub3BhY2l0eSA+IDApIHtcbiAgICAgICAgICAgICAgICAod2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZSAmJiByZXF1ZXN0QW5pbWF0aW9uRnJhbWUodGljaykpIHx8IHNldFRpbWVvdXQodGljaywgMTYpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBpZih0eXBlb2YgY2IgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgICAgICAgICAgY2IoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgdGljaygpO1xuICAgIH1cblxuICAgIGNyZWF0ZUltYWdlKHVybCwgZmlsdGVySWQpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY3JlYXRlRWxlbWVudCgnaW1hZ2UnLCB7XG4gICAgICAgICAgICB4OiAwLFxuICAgICAgICAgICAgeTogMCxcbiAgICAgICAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICAgICAgICBoZWlnaHQ6ICcxMDAlJyxcbiAgICAgICAgICAgICdleHRlcm5hbFJlc291cmNlc1JlcXVpcmVkJzogJ3RydWUnLFxuICAgICAgICAgICAgaHJlZjogdXJsLFxuICAgICAgICAgICAgc3R5bGU6ICdmaWx0ZXI6dXJsKCMnICsgZmlsdGVySWQgKyAnKScsIC8vZmlsdGVyIGxpbmtcbiAgICAgICAgICAgIHByZXNlcnZlQXNwZWN0UmF0aW86ICd4TWlkWU1pZCBzbGljZSdcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgY3JlYXRlQmx1cigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY3JlYXRlRWxlbWVudCgnZmVHYXVzc2lhbkJsdXInLCB7XG4gICAgICAgICAgICAnaW4nOiAnU291cmNlR3JhcGhpYycsXG4gICAgICAgICAgICBzdGREZXZpYXRpb246IDEwXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGNyZWF0ZUZpbHRlcihmaWx0ZXJJZCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jcmVhdGVFbGVtZW50KCdmaWx0ZXInLCB7XG4gICAgICAgICAgICBpZDogZmlsdGVySWRcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgY3JlYXRlU1ZHKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jcmVhdGVFbGVtZW50KCdzdmcnLCB7XG4gICAgICAgICAgICB3aWR0aDogJzEwMCUnLFxuICAgICAgICAgICAgaGVpZ2h0OiAnMTAwJScsXG4gICAgICAgICAgICB4bWxuczogJ2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyxcbiAgICAgICAgICAgIHZlcnNpb246ICcxLjEnLFxuICAgICAgICAgICAgc3R5bGU6IFwicG9zaXRpb246IGFic29sdXRlOyB6LWluZGV4OiAxOyBiYWNrZ3JvdW5kOiB3aGl0ZTsgbGVmdDogMDsgdG9wOiAwO1wiXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGNyZWF0ZUVsZW1lbnQobmFtZSwgYXR0cnMpIHtcbiAgICAgICAgbGV0IGVsID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudE5TKHRoaXMuc3ZnbnMsIG5hbWUpO1xuXG4gICAgICAgIGlmIChhdHRycykge1xuICAgICAgICAgICAgdGhpcy5zZXRTVkdBdHRyKGVsLCBhdHRycyk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gZWw7XG4gICAgfVxuXG4gICAgc2V0U1ZHQXR0cihlbCwgYXR0cnMpIHtcbiAgICAgICAgZm9yIChsZXQgaSBpbiBhdHRycykge1xuICAgICAgICAgICAgaWYgKGkgPT09ICdocmVmJykge1xuICAgICAgICAgICAgICAgIGVsLnNldEF0dHJpYnV0ZU5TKHRoaXMueGxpbmssIGksIGF0dHJzW2ldKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgZWwuc2V0QXR0cmlidXRlKGksIGF0dHJzW2ldKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBlbDtcbiAgICB9XG5cbiAgICBnZW5lcmF0ZUZpbHRlcklkKCkge1xuICAgICAgICBsZXQgaWQgPSAnZmlsdGVyXycrIERhdGUubm93KCk7XG4gICAgICAgIHJldHVybiBpZDtcbiAgICB9XG59XG5cbndpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdsb2FkJywgKCkgPT4ge1xuICAgIGZvckVhY2goZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnW2RhdGEtbGF6eS1iZ10nKSwgaXRlbSA9PiB7XG4gICAgICAgIG5ldyBCbHVyQmcoaXRlbSk7XG4gICAgfSk7XG59KTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gc3JjL2pzL2FwcC5qcyJdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBOzs7Ozs7Ozs7OztBQ2hFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBOzs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBUkE7QUFVQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBOzs7QUFFQTtBQUNBO0FBQ0E7QUFEQTtBQUdBOzs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUxBO0FBT0E7OztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7Ozs7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7QSIsInNvdXJjZVJvb3QiOiIifQ==