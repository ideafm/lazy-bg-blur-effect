// foreach fixes
let forEach = function (array, callback, scope) {
    for (var i = 0; i < array.length; i++) {
        callback.call(scope, array[i], i);
    }
};

// lazy bg blur effect
class BlurBg {
    constructor(rootEl) {

        this.svgns = 'http://www.w3.org/2000/svg';
        this.xlink = 'http://www.w3.org/1999/xlink';

        this.root = rootEl;
        this.img = rootEl.getAttribute('data-lazy-bg');
        this.imgThumb = this.getThumbName();

        let svg = this.createSVG();
        let filterId = this.generateFilterId();
        let filter = this.createFilter(filterId);
        let blur = this.createBlur();
        let image = this.createImage(this.imgThumb, filterId);

        filter.appendChild(blur);
        svg.appendChild(filter);
        svg.appendChild(image);

        rootEl.appendChild(svg);

        this.createImg(svg);

    }

    getThumbName() {
        let thumb = this.root.getAttribute('data-lazy-bg-thumb');
        if(!thumb) {
            let res = this.img.match(/\.[png|jpg|svg|jpeg]+$/);
            if(res) {
                thumb = this.img.substr(0, res.index) + '_thumb' + res[0];
            }
        }
        return thumb;
    }

    createImg(svg) {
        let img = new Image();
        img.addEventListener('load', e => {
            this.root.style.backgroundImage = `url('${this.img}')`;
            setTimeout(() => {
                this.fadeOut(svg, e => {
                    svg.parentNode.removeChild(svg);
                })
            }, 500)
        });
        img.setAttribute('src', this.img);
    }

    fadeOut(el, cb) {
        el.style.opacity = 1;

        let last = +new Date();
        let tick = function() {
            el.style.opacity = +el.style.opacity - (new Date() - last) / 600;
            last = +new Date();

            if (+el.style.opacity > 0) {
                (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
            } else {
                if(typeof cb === 'function') {
                    cb();
                }
            }
        };

        tick();
    }

    createImage(url, filterId) {
        return this.createElement('image', {
            x: 0,
            y: 0,
            width: '100%',
            height: '100%',
            'externalResourcesRequired': 'true',
            href: url,
            style: 'filter:url(#' + filterId + ')', //filter link
            preserveAspectRatio: 'xMidYMid slice'
        });
    }

    createBlur() {
        return this.createElement('feGaussianBlur', {
            'in': 'SourceGraphic',
            stdDeviation: 10
        });
    }

    createFilter(filterId) {
        return this.createElement('filter', {
            id: filterId
        });
    }

    createSVG() {
        return this.createElement('svg', {
            width: '100%',
            height: '100%',
            xmlns: 'http://www.w3.org/2000/svg',
            version: '1.1',
            style: "position: absolute; z-index: 1; background: white; left: 0; top: 0;"
        });
    }

    createElement(name, attrs) {
        let el = document.createElementNS(this.svgns, name);

        if (attrs) {
            this.setSVGAttr(el, attrs);
        }

        return el;
    }

    setSVGAttr(el, attrs) {
        for (let i in attrs) {
            if (i === 'href') {
                el.setAttributeNS(this.xlink, i, attrs[i]);
            } else {
                el.setAttribute(i, attrs[i]);
            }
        }

        return el;
    }

    generateFilterId() {
        let id = 'filter_'+ Date.now();
        return id;
    }
}

window.addEventListener('load', () => {
    forEach(document.querySelectorAll('[data-lazy-bg]'), item => {
        new BlurBg(item);
    });
});